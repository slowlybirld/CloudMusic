# -*- coding: utf-8 -*-
import xbmcgui, xbmc

ACTION_MOVE_LEFT = 1
ACTION_MOVE_RIGHT = 2
ACTION_MOVE_UP = 3
ACTION_MOVE_DOWN = 4
ACTION_PAGE_UP = 5
ACTION_PAGE_DOWN = 6
ACTION_SELECT_ITEM = 7
ACTION_HIGHLIGHT_ITEM = 8
ACTION_PARENT_DIR_OLD = 9
ACTION_PARENT_DIR = 92
ACTION_PREVIOUS_MENU = 10
ACTION_SHOW_INFO = 11
ACTION_PAUSE = 12
ACTION_STOP = 13
ACTION_NEXT_ITEM = 14
ACTION_PREV_ITEM = 15
ACTION_SHOW_GUI = 18
ACTION_PLAYER_PLAY = 79
ACTION_MOUSE_LEFT_CLICK = 100
ACTION_MOUSE_MOVE = 107
ACTION_CONTEXT_MENU = 117

class VstSession:
    def __init__(self, window=None):
        self.window = window

    def removeCRLF(self, text):
        return " ".join(text.split())

    def makeAscii(self, name):
        return name.encode('ascii', 'replace')

    def closeWindow(self):
        self.window.doClose()

    #def clearSetting(self, key):
    #    __addon__.setSetting(key, '')

    #def setSetting(self, key, value):
    #    __addon__.setSetting(key, value and ENCODE(value) or '')

    #def getSetting(self, key, default=None):
    #    setting = __addon__.getSetting(key)
    #    if not setting: return default
    #    if type(default) == type(0):
    #         return int(float(setting))
    #     elif isinstance(default, bool):
    #         return setting == 'true'
    #     return setting

class BaseWindowDialog(xbmcgui.WindowXMLDialog):
    def __init__(self, *args, **kwargs):
        self.session = None
        self.oldWindow = None
        self.busyCount = 0
        xbmcgui.WindowXML.__init__(self)

    def doClose(self):
        self.session.window = self.oldWindow
        self.close()

    def onInit(self):
        # if player.isPlaying():
        #     player.stop()
        if self.session:
            self.session.window = self
        else:
            try:
                self.session = VstSession(self)
            except:
                self.close()
        self.setSessionWindow()

    def onFocus(self, controlId):
        self.controlId = controlId

    def setSessionWindow(self):
        try:
            self.oldWindow = self.session.window
        except:
            self.oldWindow = self
        self.session.window = self

    def onAction(self, action):
        if action.getId() == ACTION_PARENT_DIR or action.getId() == ACTION_PREVIOUS_MENU:
            # if player.isPlaying():
            #     player.stop()
            self.doClose()
        else:
            return False

        return True

    def showBusy(self):
        if self.busyCount > 0:
            self.busyCount += 1
        else:
            self.busyCount = 1
            xbmc.executebuiltin("ActivateWindow(busydialog)")

    def hideBusy(self):
        if self.busyCount > 0:
            self.busyCount -= 1
        if self.busyCount == 0:
            xbmc.executebuiltin("Dialog.Close(busydialog)")


class ConfirmWindow(BaseWindowDialog):
    def __init__(self, *args, **kwargs):
        self.selected = -1
        BaseWindowDialog.__init__(self)

    def onClick(self, controlId):
        BaseWindowDialog.onClick(self, controlId)
        if (controlId == 1410):
            self.selected = 0
            self.doClose()
        elif (controlId == 1411):
            self.selected = 1
            self.doClose()

    def select(self):
        self.doModal()
        return self.selected


class BaseWindow(xbmcgui.WindowXML):
    def __init__(self, *args, **kwargs):
        self.session = None
        self.oldWindow = None
        self.busyCount = 0
        xbmcgui.WindowXML.__init__(self)

    def doClose(self):
        self.session.window = self.oldWindow
        self.close()

    def onInit(self):
        if self.session:
            self.session.window = self
        else:
            try:
                self.session = VstSession(self)
            except:
                self.close()
        self.setSessionWindow()

    def onFocus(self, controlId):
        self.controlId = controlId

    def setSessionWindow(self):
        try:
            self.oldWindow = self.session.window
        except:
            self.oldWindow = self
        self.session.window = self

    def onAction(self, action):
        if action.getId() == ACTION_PARENT_DIR or action.getId() == ACTION_PREVIOUS_MENU:
            self.doClose()
        else:
            return False

        return True

    def showBusy(self):
        if self.busyCount > 0:
            self.busyCount += 1
        else:
            self.busyCount = 1
            xbmc.executebuiltin("ActivateWindow(busydialog)")

    def hideBusy(self):
        if self.busyCount > 0:
            self.busyCount -= 1
        if self.busyCount == 0:
            xbmc.executebuiltin("Dialog.Close(busydialog)")

# -*- coding: utf-8 -*-
import sys
import os
import socket
import urllib2

import xbmcaddon
import xbmc

try:
    import StorageServer
except:
    import storageserverdummy as StorageServer

__addonid__ = "plugin.audio.cloudMusic"
__addon__ = xbmcaddon.Addon(id=__addonid__)
cwd = __addon__.getAddonInfo('path')
__profile__ = xbmc.translatePath(__addon__.getAddonInfo('profile'))
__resource__ = xbmc.translatePath(os.path.join(cwd, 'libs'))
sys.path.append(__resource__)

socket.setdefaulttimeout(10)
cache = StorageServer.StorageServer(__addonid__, 87600)
cookie = urllib2.HTTPCookieProcessor()

def log(msg):
    print ('[CloudMusic]%s' % msg)